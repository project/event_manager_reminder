
Event Manager Block Reminder provides a cron hook to send reminders for events
that you volunteered for. It extends both Event (http://drupal.org/project/event)
and Event Manager (http://drupal.org/project/event_manager)

Installation
------------

Copy event_manager_reminder to your module directory (either /modules or 
/sites/default/modules) and then enable on the admin modules page. Configuration
options live under admin/settings/event_manager/reminder.

Author
------
Mel Martin
mmartin@computer.org